# "Game of Thrones" quotes app.

This app works with ["Game of Thrones" quotes API](https://gameofthronesquotes.xyz/) and it's builded with React JS.


## Description

In this project we follow a design provided for the [frontendmentor](https://www.frontendmentor.io/) challenge ([link](https://www.frontendmentor.io/challenges/advice-generator-app-QdUG-13db)), using Use State hook and async / await function for consume the API.

[Link to site](https://gameofthrones-quotes.netlify.app/)

Please enyoy it!!

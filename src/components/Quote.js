import { useState } from "react";
import iconDice from '../img/icon-dice.svg';
import pattern from '../img/pattern-divider-mobile.svg';
import './Quotes.css'


const URL = 'https://api.gameofthronesquotes.xyz/v1/random';

const Quote = () => {

    const [sentence, setSentence] = useState({
        character: 'ARYA STARK',
        sentence: '"Leave one wolf alive and the sheep are never safe."'
    });

    const getQuote = async (url) => {
        const response = await fetch(url);
        const data = await response.json();
        
        console.log(data)

        setSentence({
            character: (data.character.name).toUpperCase(),
            sentence: `"${data.sentence}"`
        });
    };

    const handleClick = () => {
        getQuote(URL);
    };

    return (
        <div className="container">
            <div className="character">
                {sentence.character} says:
            </div>
            <div className="sentence">
                {sentence.sentence}
            </div>
            <div className="pattern">
                <img
                    src={pattern}
                    alt="pattern" />
            </div>
            <button 
                className="btn"
                onClick={handleClick} >
                    <img 
                        src={iconDice}
                        alt="icon" />
            </button>
        </div>
    );
};

export default Quote;